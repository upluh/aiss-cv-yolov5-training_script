- [About this file](#about-this-file)
- [Getting Started](#getting-started)
  - [First steps](#first-steps)
  - [Structure of the repository](#structure-of-the-repository)
  - [Config files in yolov5](#config-files-in-yolov5)
    - [Data config files](#data-config-files)
    - [Model config files](#model-config-files)
  - [Starting a job](#starting-a-job)
  - [Creating a new job script](#creating-a-new-job-script)
    - [What the job template script does](#what-the-job-template-script-does)
  - [After job(s) finished](#after-jobs-finished)
- [Results](#results)

# About this file

This file explains a few useful features and the way scripts have been added to use the hpc cluster. It will not document details regarding yolov5 (see it's [documentation here](https://docs.ultralytics.com/yolov5/)) It will not go into details that are already explained in the [documentation of the bwUniCluster2.0](https://wiki.bwhpc.de/e/BwUniCluster2.0).

# Getting Started

## First steps

Clone this repository and in it the yolov5 repository either directly into your home directory of the cluster, or onto your host machine and copy it to the cluster (via scp, sftp or rsync). You can either already download the data (from roboflow) and put it into the `data/` folder, or do it later. Remember to adjust the rsync if you want to sync another specific folder.

> The data should contain a minimum of `vx_x` of versioning. This is important, because the data file config will be named after the data version. This makes it easier to keep track of the data and the results.

```bash
# Clone the repo from https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5.git
git clone https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5.git
cd yolov5

# Clone the yolov5 repo
git clone https://github.com/ultralytics/yolov5
cd yolov5
git reset --hard 064365d8683fd002e9ad789c1e91fa3d021b44f0

cd ../../

# copy the repo to the cluster if not cloned there directly
rsync -rPzhv --info=progress2 yolov5/ <username>@bwunicluster.scc.kit.edu:~/yolov5
```

## Structure of the repository

```bash
.
├── clean_up_outputs.sh
├── data # should contain all the data (for more structure)
│   ├── data_v1_0 # should contain versioning of type vx_x and be in sync with data versioning on all tools
│   │   └── ...
│   └── data_v1_1_wo_augmentation
│       └── ...
├── jobs # contains the job scripts (⚠️ call from the root directory of the repository)
│   ├── ... # your job scripts (f.e. copied from job_template.sh)
│   └── job_template.sh
├── outputs_jobs # ../clean_up_outputs.sh will rename and move the output files here
│   └── ...
└── yolov5 # the yolov5 repository (this is not tracked as a submodule in git)
    ├── ...
    ├── data # these are the data config files
    │   ├── ...
    │   ├── data_v1_0.yaml
    │   └── data_v1_1_wo_augmentation.yaml
    ├── detect.py
    ├── export.py
    ├── models # these are the model config files
    │   ├── ...
    │   ├── yolov5l.yaml
    │   ├── yolov5m.yaml
    │   ├── yolov5n.yaml
    │   ├── yolov5s.yaml
    │   ├── yolov5s_roboflow.yaml
    │   └── yolov5x.yaml
    ├── requirements.txt
    ├── runs
    │   └── train # these contain results of training
    └── train.py
```

## Config files in yolov5

Since the yolov5 repository is it's separate git repository, files in it will not be tracked for the main repository. So here is an example of the config files in the yolov5 repository.

### Data config files

```yaml
path: ../data/data_v1_0
train: train/images
val: valid/images
test: test/images

nc: 31
names:
  [
    "black_short",
    "black_stick_long",
    "blue_long",
    "blue_short",
    "engine",
    "gray_45",
    "gray_90",
    "gray_frame",
    "gray_long",
    "gray_loop",
    "gray_short",
    "green_3",
    "red_shaft",
    "state_1",
    "state_10",
    "state_11",
    "state_12",
    "state_13",
    "state_14",
    "state_15",
    "state_2",
    "state_3",
    "state_4",
    "state_5",
    "state_6",
    "state_7",
    "state_8",
    "state_9",
    "tire",
    "wheel",
    "white_45",
  ]

roboflow:
  workspace: aisscv-ffp8r
  project: aiss_cv-sx8yb
  version: 1
  license: CC BY 4.0
  url: https://universe.roboflow.com/aisscv-ffp8r/aiss_cv-sx8yb/dataset/1
```

### Model config files

Here, you can for example also use the preset defined by yolov5 (f.e. `yolov5s`, `yolov5m`, `yolov5l`, `yolov5x`).

```yaml
# parameters
nc: 31 # number of classes
depth_multiple: 0.33 # model depth multiple
width_multiple: 0.50 # layer channel multiple

# anchors
anchors:
  - [10, 13, 16, 30, 33, 23] # P3/8
  - [30, 61, 62, 45, 59, 119] # P4/16
  - [116, 90, 156, 198, 373, 326] # P5/32

# YOLOv5 backbone
backbone:
  # [from, number, module, args]
  [
    [-1, 1, Focus, [64, 3]], # 0-P1/2
    [-1, 1, Conv, [128, 3, 2]], # 1-P2/4
    [-1, 3, BottleneckCSP, [128]],
    [-1, 1, Conv, [256, 3, 2]], # 3-P3/8
    [-1, 9, BottleneckCSP, [256]],
    [-1, 1, Conv, [512, 3, 2]], # 5-P4/16
    [-1, 9, BottleneckCSP, [512]],
    [-1, 1, Conv, [1024, 3, 2]], # 7-P5/32
    [-1, 1, SPP, [1024, [5, 9, 13]]],
    [-1, 3, BottleneckCSP, [1024, False]], # 9
  ]

# YOLOv5 head
head: [
    [-1, 1, Conv, [512, 1, 1]],
    [-1, 1, nn.Upsample, [None, 2, "nearest"]],
    [[-1, 6], 1, Concat, [1]], # cat backbone P4
    [-1, 3, BottleneckCSP, [512, False]], # 13

    [-1, 1, Conv, [256, 1, 1]],
    [-1, 1, nn.Upsample, [None, 2, "nearest"]],
    [[-1, 4], 1, Concat, [1]], # cat backbone P3
    [-1, 3, BottleneckCSP, [256, False]], # 17 (P3/8-small)

    [-1, 1, Conv, [256, 3, 2]],
    [[-1, 14], 1, Concat, [1]], # cat head P4
    [-1, 3, BottleneckCSP, [512, False]], # 20 (P4/16-medium)

    [-1, 1, Conv, [512, 3, 2]],
    [[-1, 10], 1, Concat, [1]], # cat head P5
    [-1, 3, BottleneckCSP, [1024, False]], # 23 (P5/32-large)

    [[17, 20, 23], 1, Detect, [nc, anchors]], # Detect(P3, P4, P5)
  ]
```

## Starting a job

The hpc cluster uses the [Slurm Workload Manager](https://slurm.schedmd.com/documentation.html) to manage jobs. To start a job, one creates a job script and submits it to the cluster. The job script is a bash script that contains the commands to be executed on the cluster. There is a job script template in the `jobs` directory. See [the hpc scripts repository for more details](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts.git).

> ⚠️ Remember to always call the jobs from the root directory of the repository, otherwise the relative paths will not be correct.

```bash
sbatch -p <partition> jobs/<job_script>.sh
```

To see available partitions, use `sinfo`:

```bash
sinfo_t_idle
```

## Creating a new job script

To create a new job script, simply copy the template and adjust it to your needs. It is highly recommended to create a separate job script for each task, as this makes it easier to keep track of the jobs and their progress. Later, they can be removed if the job was successful.

```bash
cp jobs/template.sh jobs/<job_script>.sh
```

The job template contains the most important options as variables at the top of the script:

```bash
#...

# Configuration variables
IMG_SIZE=640
BATCH_SIZE=128
EPOCHS=300
DATA_FILE=data/data_v1_0.yaml
CFG_FILE=models/yolov5s_roboflow.yaml
WEIGHTS_FILE=yolov5s.pt
AMOUNT_DEVICES=2
CACHE_ENABLED=true

#...
```

Use them, to adjust the yolov5 job. Here is a short explanation of them:

| Variable         | Description                                                                                                                                                                                                       |
| ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `IMG_SIZE`       | The size of the input images.                                                                                                                                                                                     |
| `BATCH_SIZE`     | The batch size.                                                                                                                                                                                                   |
| `EPOCHS`         | The number of epochs to train.                                                                                                                                                                                    |
| `DATA_FILE`      | The path to the data file config (recommendation: create a separate one for each data version and use versioning for them in roboflow).                                                                           |
| `CFG_FILE`       | The path to the model config file (recommendation: the filename should follow this structure yolov5<size>\_<additionalNotes>.yaml).                                                                               |
| `WEIGHTS_FILE`   | The path to the weights file. Popular examples: `yolov5s.pt`, `""`                                                                                                                                                |
| `AMOUNT_DEVICES` | The amount of devices to use. ⚠️ These also have to be changed in the comments above that let slurm know, how many ressources we request (and is therefore the only attribute that needs to be set in two places) |
| `CACHE_ENABLED`  | Whether to use the cache or not.                                                                                                                                                                                  |

The name of the run will be deferred from these options. It will follow this structure: `<data_version_number>_<imgSize>x_<batchSize>b_<epochs>e_<weights>W_<shortFormModel>-model`. For example: `v1_0_640x_128b_4e_V5sW_v5s_roboflow-model`. This makes it easy to identify the run and its parameters.

### What the job template script does

> The script template is meant to be used by only changing the parameter variables at its top. This section solely serves as an explanation of what the script does. Changes of the script logic should be done with caution and at the developers discretion.

The `jobs/job_template.sh` script is a template for any new jobs. In it, the hpc checks for `conda` and loads the software module for conda (see [software modules on hpc](https://wiki.bwhpc.de/e/BwUniCluster2.0/Software_Modules)) if it is not yet available. Then, it checks for the necessary dependencies and installs them, if they are not yet available in the conda environment. Then, the conda environment is loaded and training starts with the given parameters as [described above](#creating-a-new-job-script). Read more about the job script template and its individual sections in the [hpc scripts repository](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/hpc_scripts.git).

## After job(s) finished

Since each job in slurm produces `slurm-<job_id>.out` files which are not easily assignable to the job and it's attributes, the `clean_up_output.sh` script helps and will automatically find the name of the job and rename the `*.out` files to the name that was passed to yolov5. They will therefore correspond to the results that are in the `yolov5/runs/train` directory.

```bash
./clean_up_output.sh
```

By default, the script contains the `copy_to_runs_dir=true` flag which will also copy the file into the respective results directory at `yolov5/runs/train/<job_name>`. This is useful, if the `*.out` file should be kept with the training results and moved to a repository later. If this is not desired, the flag can be set to `false`.

# Results

Results produced by the [BwUniCluster2.0](https://wiki.bwhpc.de/e/BwUniCluster2.0) are kept in a separate repo here: [https://git.scc.kit.edu/aiss_cv/yolo_models/yolov5.git](https://git.scc.kit.edu/aiss_cv/yolo_models/yolov5.git).
