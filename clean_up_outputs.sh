#!/bin/bash

directory="."  # Replace with your target directory of source *.out files
target_directory="outputs_jobs"
# If true, copies the files to yolov5/runs/train/<directory_name>, deactivate if not needed
copy_to_runs_dir=true

# Iterate over all files with .out extension in the directory
for file in "$directory"/*.out; do
    # Get the filename without the directory path
    filename=$(basename "$file")

    # Extract the directory name from the last 20 lines of the file
    directory=$(tail -n 20 "$file" | grep -oP 'runs/train/\K[^/]+')

    # Remove special characters and escape sequences from the directory name
    directory=$(echo "$directory" |  sed -E 's/[^[:alnum:]_-]|'$'\033\[0m''//g')

    # Generate the new filename
    new_filename="$directory.out"

    # to control break from if statement
    found_file=false

    # Rename the file
     if [ "$copy_to_runs_dir" = true ]; then
        additional_directory="yolov5/runs/train/$directory"
        additional_target="$additional_directory/$new_filename"

        if [ -d "$additional_directory" ]; then
                found_file=true
        else
                echo "Directory $additional_directory not found."
        fi

        if [ "$found_file" = true ]; then
                cp "$file" "$additional_target"
                echo "Copied $filename to $additional_target"
        fi
    fi
    mv "$file" "$target_directory/$new_filename"

    echo "Moved $filename to $target_directory/$new_filename"
done
