#!/bin/bash

# use ENV_NAME that is passed down from job_template.sh, otherwise use "base"
ENV_NAME="${ENV_NAME:-base}"

module purge
module load devel/miniconda/4.9.2

source ~/.bashrc
# check, if conda can be loaded via custom function
if command -v conda_init >/dev/null 2>&1; then
    echo "conda_init is available."
else
    echo "conda_init is not available. Adding to .bashrc..."
    cat jobs/conda_config/custom_conda_init.sh >> ~/.bashrc
    echo "conda_init has been added to .bashrc and can be used from now on."
fi

source ~/.bashrc
conda_init

# Check if $ENV_NAME environment exists
if conda env list | grep -qi "^$ENV_NAME\s"; then
  echo "Conda environment '$ENV_NAME' already exists, loading it..."
  conda activate "$ENV_NAME"

  echo "Conda environment '$ENV_NAME' loaded."
else
  # Create $ENV_NAME environment if it does not exist
  echo "Creating Conda environment '$ENV_NAME'..."
  conda create -n "$ENV_NAME"
  echo "Conda environment '$ENV_NAME' created. Installing dependencies..."

  conda activate "$ENV_NAME"
  #
  # Instructions for installing dependencies here
  #
  # Create "yolov5" environment
  echo "Creating Conda environment 'yolov5'..."
  conda create -n yolov5
  echo "Conda environment 'yolov5' created. Installing dependencies..."
  conda activate yolov5
  pip install -r yolov5/requirements.txt
  pip uninstall torch torchvision -y
  pip install torch torchvision --index-url https://download.pytorch.org/whl/cu118

  echo "Conda environment '$ENV_NAME' created, loaded and dependencies installed."
fi
