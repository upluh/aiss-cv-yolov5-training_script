#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --mem=256gb
#SBATCH --job-name=aiss_cv
#SBATCH --gres=gpu:2

#
# Conda environment name
# - will use "base" if not specified
# - will create a new environment if the specified environment does not exist
# - configure dependency installation in jobs/conda_config/handle_conda_activation.sh
#
export ENV_NAME="yolov5"
# Configuration variables
IMG_SIZE=640
BATCH_SIZE=128
EPOCHS=300
DATA_FILE=data/data_v1_0.yaml
CFG_FILE=models/yolov5s_roboflow.yaml
WEIGHTS_FILE=yolov5s.pt
AMOUNT_DEVICES=2
CACHE_ENABLED=true

# Generate device IDs list
DEVICE_IDS=$(seq -s "," 0 $((AMOUNT_DEVICES-1)))

# Extract version number from data file using regex
VERSION=$(echo "$DATA_FILE" | grep -oP 'data_v\K\d+_\d+')

# Generate ending part of the name based on weights file
ENDING=""
if [[ $WEIGHTS_FILE == "" ]]; then
    ENDING="noStartWeights"
elif [[ $WEIGHTS_FILE == yolo* ]]; then
    ENDING=$(basename "$WEIGHTS_FILE" .pt)
    ENDING=${ENDING#"yolo"}
    ENDING=${ENDING//_/}
    ENDING=${ENDING^}
else
    ENDING=$(basename "$WEIGHTS_FILE" .pt)
    ENDING=${WEIGHTS_FILE^}
fi

# Extract configuration file name without extension
CONFIG_NAME=$(basename "$CFG_FILE" .yaml)

# Remove "yolo" from the beginning of the configuration file name
CONFIG_NAME=${CONFIG_NAME#"yolo"}

# Generate name
NAME="v${VERSION}_${IMG_SIZE}x_${BATCH_SIZE}b_${EPOCHS}e_${ENDING}W_${CONFIG_NAME}-model"

# make sure conda is available and properly set up
source jobs/conda_config/handle_conda_activation.sh

# Run the command to get the active environment information
env_info=$(conda info --json)

# Check if the environment_info contains the "active_prefix" key
if [[ $env_info == *"active_prefix"* ]]; then
    # Extract the active environment name from the environment_info
    active_env=$(echo "$env_info" | jq -r '.active_prefix' | awk -F'/' '{print $NF}')
    echo "Conda environment '$active_env' is active."
else
    echo "Conda environment is not active."
fi

echo "Starting yolo 🚀"
cd yolov5

# if using multiple gpus, use python -m torch.distributed.run --nproc_per_node <# of gpus> train.py ...
python -m torch.distributed.run --nproc_per_node 2 train.py \
        --img $IMG_SIZE \
        --batch $BATCH_SIZE \
        --epochs $EPOCHS \
        --data $DATA_FILE \
        --cfg $CFG_FILE \
        $(if [[ -n "$WEIGHTS_FILE" ]]; then echo "--weights $WEIGHTS_FILE"; elif [[ "$WEIGHTS_FILE" == "" ]]; then echo "--weights \"\""; fi) \
        --device $DEVICE_IDS \
        $(if $CACHE_ENABLED; then echo "--cache"; fi) \
	--name $NAME
